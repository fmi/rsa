package com.rsa.util;

public class Timer {

    private long iterationMilliseconds;

    private long elapsedMilliseconds;

    private boolean inProgress;

    public void start() {
	elapsedMilliseconds = 0;
	iterationMilliseconds = System.currentTimeMillis();
	inProgress = true;
    }

    public void resume() {
	iterationMilliseconds = System.currentTimeMillis();
	inProgress = true;
    }

    public void stop() {
	if (inProgress) {
	    elapsedMilliseconds += System.currentTimeMillis() - iterationMilliseconds;
	}
	inProgress = false;
    }

    public long getElapsedTime() {
	if (inProgress) {
	    return System.currentTimeMillis() - iterationMilliseconds;
	} else {
	    return elapsedMilliseconds;
	}
    }

}
