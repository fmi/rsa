package com.rsa.util;

import com.rsa.cli.CliConfiguration;

public class StringFormat {

    public static String timeInMinutes(long milliseconds) {
	return String.format("%d min, %d sec, %d millis", (int) ((milliseconds / (1000 * 60)) % 60),
		(int) (milliseconds / 1000) % 60, (int) (milliseconds % 1000));
    }

    public static String ellipsize(String origin, int maxChars) {
	if (origin.length() > maxChars) {
	    return origin.substring(0, maxChars) + "...";
	}
	return origin;
    }

    public static String cliConfiguration(CliConfiguration config) {
	String quietParameter = config.quiet() ? " -q" : "";
	return String.format("Configuration is: -p %d -t %d -o %s%s", config.varTo(), config.threadCount(), config
		.output().getPath(), quietParameter);
    }

    public static String precision(long precision) {
	if (precision < 5) {
	    return "Expected precision is around <5 digits after the decimal sign";
	} else {
	    return String.format("Expected precision is around %d digits after the decimal sign", precision);
	}
    }
}
