package com.rsa.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.apfloat.Apfloat;

public interface RsaMaster extends Remote {

    void registerSlave(RsaSlave slave) throws RemoteException;

    void reportResult(Apfloat result) throws RemoteException;
}
