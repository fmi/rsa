package com.rsa.rmi.policy;

import java.security.Permission;

public class PermissiveSecurityMenager extends SecurityManager {

	@Override
	public void checkAccept(String host, int port) {
	}

	@Override
	public void checkConnect(String host, int port, Object context) {
	}

	@Override
	public void checkConnect(String host, int port) {
	}

	@Override
	public void checkListen(int port) {
	}

	@Override
	public void checkPermission(Permission perm) {
	}

	@Override
	public void checkPermission(Permission perm, Object context) {
	}

}
