package com.rsa.rmi.policy;

import java.net.SocketPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Policy;
import java.util.PropertyPermission;

public class MasterPolicy extends Policy {

    private static PermissionCollection perms;

    public MasterPolicy(int masterRmiPort) {
	super();
	if (perms == null) {
	    perms = new SimplePermissionCollection();
	    addPermissions(masterRmiPort);
	}
    }

    @Override
    public PermissionCollection getPermissions(CodeSource codesource) {
	return perms;
    }

    private void addPermissions(int masterRmiPort) {
	String port = String.valueOf(masterRmiPort);
	SocketPermission socketPermission = new SocketPermission("*:" + port + "-", "listen, resolve");
	PropertyPermission propertyPermission = new PropertyPermission("*", "read, write");

	perms.add(socketPermission);
	perms.add(propertyPermission);
    }

}
