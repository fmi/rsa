package com.rsa.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RsaSlave extends Remote {

    void execute(RemoteConfiguration config) throws RemoteException;

    int cpuCount() throws RemoteException;
}
