package com.rsa.rmi;

import java.io.Serializable;

import com.rsa.math.seq.sum.SumConfiguration;

public interface RemoteConfiguration extends Serializable, SumConfiguration {

}
