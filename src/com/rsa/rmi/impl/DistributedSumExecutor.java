package com.rsa.rmi.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apfloat.Apfloat;

import com.rsa.cli.Logger;
import com.rsa.math.seq.Sequence;
import com.rsa.math.seq.sum.SumConfiguration;
import com.rsa.math.seq.sum.SumExecutor;
import com.rsa.rmi.RsaMaster;
import com.rsa.rmi.RsaSlave;

public class DistributedSumExecutor extends UnicastRemoteObject implements RsaMaster, SumExecutor {

    private static final long serialVersionUID = -6921684413287818427L;

    private Map<RsaSlave, RemoteConfigurationBean> slaves;

    private int totalCpus;

    private Apfloat aggratedSum;

    private Object waitLock;

    private int reportedSlaves;

    public DistributedSumExecutor() throws RemoteException {
	super();
	slaves = new HashMap<>();
	aggratedSum = Apfloat.ZERO;
	waitLock = new Object();
    }

    @Override
    public void registerSlave(RsaSlave slave) throws RemoteException {
	if (slave != null) {
	    int cpuCount = slave.cpuCount();
	    slaves.put(slave, new RemoteConfigurationBean(cpuCount));
	    totalCpus += cpuCount;
	    Logger.getInstance().info("A new slave is registered, total slaves bound to this master: " + slaves.size());
	    Logger.getInstance().info("Total number of available CPUs: " + totalCpus);
	}
    }

    @Override
    public synchronized void reportResult(Apfloat result) throws RemoteException {
	aggratedSum = aggratedSum.add(result);
	reportedSlaves++;

	if (slaves.size() == reportedSlaves) {
	    synchronized (waitLock) {
		waitLock.notifyAll();
	    }
	}
    }

    @Override
    public Apfloat sum(SumConfiguration config, Sequence sequence, ProgressListener listener) {
	reportedSlaves = 0;
	aggratedSum = Apfloat.ZERO;

	assigneVariableValues(config);
	balanceLoad(config.threadCount());

	slaves.forEach((slave, configuration) -> {
	    try {
		slave.execute(configuration);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	});

	synchronized (waitLock) {
	    if (slaves.size() != reportedSlaves) {
		try {
		    waitLock.wait();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
	    }
	}

	return aggratedSum;
    }

    private void assigneVariableValues(SumConfiguration masterConfiguration) {
	int varStep = slaves.size();

	Collection<RemoteConfigurationBean> configs = slaves.values();
	int i = 0;
	for (RemoteConfigurationBean config : configs) {
	    config.setVarFrom(masterConfiguration.varFrom() + i);
	    config.setVarTo(masterConfiguration.varTo());
	    config.setVarStep(varStep);
	    i++;
	}
    }

    private void balanceLoad(int requestedThreadCount) {
	int availableCpus = totalCpus;
	int assignedThreads = 0;
	Collection<RemoteConfigurationBean> configs = slaves.values();

	while (availableCpus > 0 && assignedThreads <= requestedThreadCount) {
	    for (RemoteConfigurationBean config : configs) {
		if (config.getAvailableCpus() > config.threadCount()) {
		    config.setThreadCount(config.threadCount() + 1);
		    availableCpus--;
		    assignedThreads++;
		}
	    }
	}

	while (assignedThreads <= requestedThreadCount) {
	    for (RemoteConfigurationBean config : configs) {
		config.setThreadCount(config.threadCount() + 1);
		assignedThreads++;
	    }
	}
    }
}
