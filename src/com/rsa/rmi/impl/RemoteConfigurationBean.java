package com.rsa.rmi.impl;

import com.rsa.rmi.RemoteConfiguration;

public class RemoteConfigurationBean implements RemoteConfiguration {
    private static final long serialVersionUID = 4869415401621587689L;

    private long varFrom;

    private long varTo;

    private long varStep;

    private int threadCount;

    private int availableCpus;

    public RemoteConfigurationBean(int cpus) {
	availableCpus = cpus;
    }

    public int getAvailableCpus() {
	return availableCpus;
    }

    public void setAvailableCpus(int availableCpus) {
	this.availableCpus = availableCpus;
    }

    @Override
    public long varFrom() {
	return varFrom;
    }

    @Override
    public long varTo() {
	return varTo;
    }

    @Override
    public long varStep() {
	return varStep;
    }

    @Override
    public int threadCount() {
	return threadCount;
    }

    public void setVarFrom(long varFrom) {
	this.varFrom = varFrom;
    }

    public void setVarTo(long varTo) {
	this.varTo = varTo;
    }

    public void setVarStep(long varStep) {
	this.varStep = varStep;
    }

    public void setThreadCount(int threadCount) {
	this.threadCount = threadCount;
    }

}
