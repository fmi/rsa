package com.rsa.rmi.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.apfloat.Apfloat;

import com.rsa.cli.Logger;
import com.rsa.concurrent.ConcurrentSumExecutor;
import com.rsa.math.seq.EulersNumberSequence;
import com.rsa.rmi.RemoteConfiguration;
import com.rsa.rmi.RsaMaster;
import com.rsa.rmi.RsaSlave;
import com.rsa.util.StringFormat;
import com.rsa.util.Timer;

public class SimpleSlave extends UnicastRemoteObject implements RsaSlave {

    private static final long serialVersionUID = -6234563586231120281L;
    private RsaMaster master;

    public SimpleSlave(RsaMaster master) throws RemoteException {
	super();
	this.master = master;
    }

    @Override
    public void execute(RemoteConfiguration config) throws RemoteException {
	ConcurrentSumExecutor exec = new ConcurrentSumExecutor();

	Timer timer = new Timer();
	timer.start();
	Apfloat result = exec.sum(config, new EulersNumberSequence(config.varTo()), null);
	timer.stop();
	long elapsedTime = timer.getElapsedTime();

	Logger.getInstance().quiet("Elapsed time: " + StringFormat.timeInMinutes(elapsedTime));
	Logger.getInstance().info("Result provided to master");
	master.reportResult(result);
    }

    @Override
    public int cpuCount() throws RemoteException {
	return Runtime.getRuntime().availableProcessors();
    }

}
