package com.rsa.main;

import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatContext;
import org.apfloat.ApfloatMath;

import com.rsa.cli.CliConfiguration;
import com.rsa.cli.Logger;
import com.rsa.cli.MasterSlaveConfiguration;
import com.rsa.cli.ProgressBar;
import com.rsa.concurrent.ConcurrentSumExecutor;
import com.rsa.math.seq.EulersNumberSequence;
import com.rsa.math.seq.sum.SumExecutor;
import com.rsa.math.seq.sum.SumExecutor.ProgressListener;
import com.rsa.rmi.RsaMaster;
import com.rsa.rmi.impl.DistributedSumExecutor;
import com.rsa.rmi.impl.SimpleSlave;
import com.rsa.rmi.policy.PermissiveSecurityMenager;
import com.rsa.util.StringFormat;
import com.rsa.util.Timer;

public class Main {

    private static class ProgressBarUpdateListener implements ProgressListener {
	private ProgressBar progressBar;

	public ProgressBarUpdateListener() {
	    progressBar = new ProgressBar();
	}

	@Override
	public void onProgressUpdate(long current, long total) {
	    progressBar.update(current, total);
	}
    }

    private static Logger logger = Logger.getInstance();

    public static void main(String[] args) {
	ApfloatContext.getGlobalContext().setNumberOfProcessors(1);
	MasterSlaveConfiguration msConfig = new MasterSlaveConfiguration(args);

	if (msConfig.isSlave()) {
	    System.setSecurityManager(new PermissiveSecurityMenager());
	    try {
		Registry registry = LocateRegistry.getRegistry(msConfig.masterRmiAddress(), msConfig.masterRmiPort());
		RsaMaster master = (RsaMaster) registry.lookup("RsaMaster");
		master.registerSlave(new SimpleSlave(master));
		logger.info("Slave started, waiting for instructions from master");

	    } catch (RemoteException | NotBoundException e) {
		logger.warning("Rsa slave could not be created.");
		e.printStackTrace();
	    }
	} else {
	    CliConfiguration config = null;
	    SumExecutor executor = null;
	    if (msConfig.isMaster()) {
		System.setSecurityManager(new PermissiveSecurityMenager());
		try {
		    executor = new DistributedSumExecutor();
		    Registry registry = LocateRegistry.createRegistry(msConfig.masterRmiPort());
		    registry.rebind("RsaMaster", (RsaMaster) executor);
		    logger.info("Master created at port " + msConfig.masterRmiPort());
		    logger.info("When all slaves are connected provide program arguments");
		    
		    boolean waitForInput = true;
		    while (waitForInput) {
			Scanner scn = null;
			try {
			    scn = new Scanner(System.in);
			    String input = scn.nextLine();
			    String[] interactiveArgs = input.split(" ");
			    config = new CliConfiguration(interactiveArgs);
			    waitForInput = false;
			} finally {
			    if (scn != null) {
				scn.close();
			    }
			}
		    }

		} catch (RemoteException e) {
		    logger.warning("Rsa Master could not be created.");
		    e.printStackTrace();
		}

	    } else {
		executor = new ConcurrentSumExecutor();
		config = new CliConfiguration(args);
	    }
	    logger.info(StringFormat.cliConfiguration(config));

	    long executionsCount = config.varTo();

	    int precision = (int) EulersNumberSequence.getPrecision(executionsCount);
	    logger.info(StringFormat.precision(precision));

	    ProgressListener progressListener = null;
	    if (!config.quiet()) {
		progressListener = new ProgressBarUpdateListener();
	    }

	    Timer timer = new Timer();
	    timer.start();
	    Apfloat result = executor.sum(config, new EulersNumberSequence(config.varTo()), progressListener);
	    result = ApfloatMath.round(result, precision - 1, RoundingMode.DOWN);
	    timer.stop();
	    long elapsedTime = timer.getElapsedTime();

	    logger.quiet("Elapsed time: " + StringFormat.timeInMinutes(elapsedTime));
	    logger.info("Result is: " + StringFormat.ellipsize(result.toString(), 50));

	    try {
		FileWriter writer = new FileWriter(config.output());
		writer.write(result.toString());
		writer.close();
		logger.info("Result written in " + config.output().getPath());
	    } catch (IOException e) {
		logger.warning("Could not write to output file");
	    }
	    System.exit(0);
	}
    }
}
