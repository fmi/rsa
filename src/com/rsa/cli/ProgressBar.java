package com.rsa.cli;

import com.rsa.cli.Logger.Level;
import com.rsa.math.MathUtils;

public class ProgressBar {

    private Logger logger;

    public ProgressBar() {
	logger = Logger.getInstance();
    }

    public void update(long current, long total) {
	int progress = (int) MathUtils.percent(current, total);
	logger.log(getProgressBarRepresentation(progress), Level.INFO, false);
    }

    private String getProgressBarRepresentation(int progress) {
	StringBuilder sb = new StringBuilder();
	sb.append('{');
	for (int i = 0; i < 100; i += 10) {
	    if (progress > i) {
		sb.append('#');
	    } else {
		sb.append(' ');
	    }
	}
	sb.append("} ").append(progress).append('%');
	return sb.toString();
    }
}
