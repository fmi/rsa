package com.rsa.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.google.common.net.HostAndPort;
import com.rsa.cli.Logger.Level;

public class MasterSlaveConfiguration {
    private boolean isMaster;

    private int masterRmiPort;

    private boolean isSlave;

    private String masterRmiAddress;
    
    private Options options;
    
    private Logger logger;

    public MasterSlaveConfiguration(String[] args) {
	logger = Logger.getInstance();
	initOptions();
	CommandLineParser parser = new DefaultParser();

	try {
	    CommandLine cli = parser.parse(options, args);
	    initValues(cli);
	} catch (ParseException e) {
	    throw new IllegalArgumentException("Illigal Arguments provided");
	}

    }
    
    private void initOptions() {
	options = new Options();
	options.addOption(Option.builder("t").hasArg().type(Number.class).longOpt("threads")
		.desc("Specify the number of threads to use").build());
	options.addOption(Option.builder("q").longOpt("quiet").desc("Run the program in quiet mode").build());
	options.addOption(Option.builder("o").longOpt("output-file").type(String.class).hasArg()
		.desc("Specify the output file").build());
	options.addOption(Option.builder("p").longOpt("precision").hasArg().type(Number.class)
		.desc("Specify the precision of the computation").build());
	options.addOption(Option.builder("m").longOpt("master").hasArg()
		.desc("Run the instance as master at the specified port").type(Number.class).build());
	options.addOption(Option.builder("sf").longOpt("slave-for").hasArg()
		.desc("Run the instance as slave for master at the specified address").type(String.class).build());
    }
    
    private void initValues(CommandLine cli) {
	if (cli.hasOption("q")) {
	    logger.setLevel(Level.QUIET);
	}

	if (cli.hasOption("m")) {
	    isMaster = true;

	    try {
		masterRmiPort = ((Number) cli.getParsedOptionValue("m")).intValue();
		if (masterRmiPort < 0 && masterRmiPort > 9999) {
		    masterRmiPort = 5151;
		    logger.warning("Invalid parameter specified for master rmi port [-m], using default value: "
			    + masterRmiPort);
		}
	    } catch (ParseException e) {
		masterRmiPort = 5151;
		logger.warning("Invalid parameter specified for master rmi port [-m], using default value: "
			+ masterRmiPort);
	    }
	} else if (cli.hasOption("sf")) {
	    isSlave = true;

	    try {
		String fullAddress = (String) cli.getParsedOptionValue("sf");
		if (fullAddress.isEmpty() || !fullAddress.contains(":")) {
		    fullAddress = "localhost:5151";
		    logger.warning("Invalid parameter specified for slave for master address [-sf], using default value: "
			    + fullAddress);
		}
		HostAndPort hap = HostAndPort.fromString(fullAddress);
		masterRmiAddress = hap.getHostText();
		masterRmiPort = hap.getPort();
		if (masterRmiPort < 0 && masterRmiPort > 9999) {
		    masterRmiPort = 5151;
		    logger.warning("Invalid parameter specified for slave for master address port [-sf], using default value: "
			    + masterRmiPort);
		}

	    } catch (ParseException | IllegalArgumentException e) {
		masterRmiAddress = "localhost";
		masterRmiPort = 5151;
		logger.warning("Invalid parameter specified for slave for master address [-sf], using default value: "
			+ masterRmiAddress);
	    }

	}
    }
    
    public boolean isMaster() {
	return isMaster;
    }

    public int masterRmiPort() {
	return masterRmiPort;
    }

    public boolean isSlave() {
	return isSlave;
    }

    public String masterRmiAddress() {
	return masterRmiAddress;
    }
}
