package com.rsa.cli;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.rsa.cli.Logger.Level;
import com.rsa.math.seq.sum.SumConfiguration;

public class CliConfiguration implements SumConfiguration {

    private boolean quiet;

    private int threadCount;

    private File output;

    private long precision;

    private Options options;

    private Logger logger;

    public CliConfiguration(String[] args) {
	logger = Logger.getInstance();
	initOptions();
	CommandLineParser parser = new DefaultParser();

	try {
	    CommandLine cli = parser.parse(options, args);
	    initValues(cli);
	} catch (ParseException e) {
	    throw new IllegalArgumentException("Illigal Arguments provided");
	}

    }

    private void initOptions() {
	options = new Options();
	options.addOption(Option.builder("t").hasArg().type(Number.class).longOpt("threads")
		.desc("Specify the number of threads to use").build());
	options.addOption(Option.builder("q").longOpt("quiet").desc("Run the program in quiet mode").build());
	options.addOption(Option.builder("o").longOpt("output-file").type(String.class).hasArg()
		.desc("Specify the output file").build());
	options.addOption(Option.builder("p").longOpt("precision").hasArg().type(Number.class)
		.desc("Specify the precision of the computation").build());
    }

    private void initValues(CommandLine cli) {
	quiet = cli.hasOption("q");

	if (quiet) {
	    logger.setLevel(Level.QUIET);
	}
	
	try {
	    if (cli.hasOption("t")) {
		threadCount = ((Number) cli.getParsedOptionValue("t")).intValue();
	    } else {
		threadCount = 1;
		logger.warning("Number of threads [-t] was not specified, using default value: " + threadCount);
	    }
	} catch (ParseException e) {
	    threadCount = 1;
	    logger.warning("Invalid parameter specified for number of threads [-t], using default value: "
		    + threadCount);
	}

	try {
	    if (cli.hasOption("p")) {
		precision = ((Number) cli.getParsedOptionValue("p")).intValue();
	    } else {
		precision = 10;
		logger.warning("Precision (number of cycles) [-p] was not specified, using default value: " + precision);
	    }
	} catch (ParseException e) {
	    precision = 10;
	    logger.warning("Invalid parameter specified for precision (number of cycles) [-p], using default value: "
		    + precision);
	}

	try {
	    if (cli.hasOption("o")) {
		String fileName = (String) cli.getParsedOptionValue("o");
		output = new File(fileName);
	    } else {
		output = new File("output.txt");
		logger.warning("Output file [-o] was not specified, using default value: " + output.getPath());
	    }
	} catch (ParseException e) {
	    output = new File("output.txt");
	    logger.warning("Invalid parameter specified for output file [-o], using default value: " + output.getPath());
	}
    }

    public boolean quiet() {
	return quiet;
    }

    public int threadCount() {
	return threadCount;
    }

    public File output() {
	return output;
    }

    public long varTo() {
	return precision;
    }

    public long varFrom() {
	return 0;
    }

    public long varStep() {
	return 1;
    }

}
