package com.rsa.cli;

public class Logger {

    public enum Level {
	INFO, WARNING, QUIET;
    }

    private static Logger instance;

    private Level currentLevel;

    private Logger() {
	currentLevel = Level.INFO;
    }

    public static Logger getInstance() {
	if (instance == null) {
	    synchronized (Logger.class) {
		if (instance == null) {
		    instance = new Logger();
		}
	    }
	}
	return instance;
    }

    public void setLevel(Level level) {
	if (level != null) {
	    currentLevel = level;
	}
    }

    public Level getLevel() {
	return currentLevel;
    }

    public void log(String message, Level logLevel, boolean persist) {
	if (currentLevel.compareTo(logLevel) <= 0) {
	    message = String.format("[%s] %s", logLevel, message);
	    if (persist) {
		System.out.println(message);
	    } else {
		System.out.print(message + '\r');
	    }
	}

    }

    public void info(String message) {
	log(message, Level.INFO, true);
    }

    public void warning(String message) {
	log(message, Level.WARNING, true);
    }

    public void quiet(String message) {
	log(message, Level.QUIET, true);
    }

}
