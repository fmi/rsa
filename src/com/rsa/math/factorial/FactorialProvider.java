package com.rsa.math.factorial;

import java.util.function.Function;

import org.apfloat.Apfloat;

@FunctionalInterface
public interface FactorialProvider extends Function<Long, Apfloat> {
}
