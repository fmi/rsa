package com.rsa.math.factorial;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.apfloat.Apfloat;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.rsa.cli.Logger;

public class CachedFactorialGenerator implements FactorialProvider {

    private Cache<Long, Apfloat> cache;

    private long maxCached;

    public CachedFactorialGenerator(int cacheSize) {
	cache = CacheBuilder.newBuilder().maximumSize(cacheSize).build();
	maxCached = 0;
	cache.put(maxCached, Apfloat.ONE);
    }

    @Override
    public synchronized Apfloat apply(Long n) {
	Apfloat result = null;
	try {
	    result = cache.get(n, new Callable<Apfloat>() {
		@Override
		public Apfloat call() {
		    return generateFactorialFromCache(n);
		}
	    });
	} catch (ExecutionException e) {
	    Logger.getInstance().warning("A computation error occured. The result will be inacurate.");
	}

	return result;
    }

    private synchronized Apfloat generateFactorialFromCache(long n) {
	Apfloat result = Apfloat.ONE;
	long nextNumber = 1;
	if (n > maxCached) {
	    Apfloat cachedResult = cache.getIfPresent(maxCached);

	    if (cachedResult != null) {
		result = cachedResult;
		nextNumber = maxCached + 1;
	    }
	}

	while (nextNumber <= n) {
	    result = result.multiply(new Apfloat(nextNumber));
	    cache.put(nextNumber, result);
	    nextNumber++;
	}

	maxCached = n;
	return result;
    }
}
