package com.rsa.math.seq;

import java.util.function.Function;

import org.apfloat.Apfloat;

@FunctionalInterface
public interface SequenceMember extends Function<Long, Apfloat> {
}