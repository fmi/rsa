package com.rsa.math.seq;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

import com.rsa.math.MathUtils;
import com.rsa.math.factorial.CachedFactorialGenerator;
import com.rsa.math.factorial.FactorialProvider;

public class EulersNumberSequence implements Sequence {

    private static class Member implements SequenceMember {

	private FactorialProvider fProvider;
	private long precision;

	public Member(long precision) {
	    fProvider = new CachedFactorialGenerator(5);
	    this.precision = precision;
	}

	@Override
	public Apfloat apply(Long n) {
	    Apfloat dividend = ApfloatMath.pow(new Apfloat(n).multiply(new Apfloat(3)), 2L).add(Apfloat.ONE);

	    Apfloat divisor = fProvider.apply(3 * n);
	    divisor = divisor.precision(precision * 2);

	    Apfloat result = dividend.divide(divisor);
	    return result;
	}

    }

    private long numberOfMembers;

    public EulersNumberSequence(long numberOfMembers) {
	this.numberOfMembers = numberOfMembers;
    }

    @Override
    public SequenceMember getMember() {
	return new Member(getPrecision(numberOfMembers));
    }

    public static long getPrecision(long numberOfMembers) {
	int digits = MathUtils.numberOfDigits(numberOfMembers);
	if (digits > 1) {
	    return (int) (numberOfMembers * ((digits - 1) * 3));
	} else if (numberOfMembers > 4) {
	    return (int) (numberOfMembers * 2) - 1;
	} else {
	    switch ((int) numberOfMembers) {
	    case 1:
		return 0;
	    case 2:
		return 0;
	    case 3:
		return 3;
	    case 4:
		return 5;
	    default:
		return 0;
	    }
	}
    }

}
