package com.rsa.math.seq.sum;

public interface SumConfiguration {
    int threadCount();

    long varFrom();

    long varTo();

    long varStep();
}
