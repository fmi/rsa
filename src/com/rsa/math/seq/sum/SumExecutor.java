package com.rsa.math.seq.sum;

import org.apfloat.Apfloat;

import com.rsa.math.seq.Sequence;

public interface SumExecutor {

    public interface ProgressListener {
	void onProgressUpdate(long current, long total);
    }

    Apfloat sum(SumConfiguration configuration, Sequence sequence, ProgressListener listener);

}
