package com.rsa.math.seq;

public interface Sequence {
    SequenceMember getMember();
}
