package com.rsa.math;

public class MathUtils {

    public static int numberOfDigits(Number number) {
	return String.valueOf(number).length();
    }

    public static double percent(double what, double outOf) {
	return (what / outOf) * 100;
    }
}
