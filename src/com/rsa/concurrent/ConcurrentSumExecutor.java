package com.rsa.concurrent;

import org.apfloat.Apfloat;

import com.rsa.math.seq.Sequence;
import com.rsa.math.seq.sum.SumConfiguration;
import com.rsa.math.seq.sum.SumExecutor;

public class ConcurrentSumExecutor implements SumExecutor {

    private ProgressListener progressListener;

    private Apfloat sum;

    public Apfloat sum(SumConfiguration configuration, Sequence sequence, ProgressListener progressListener) {
	sum = Apfloat.ZERO;
	this.progressListener = progressListener;

	Thread[] threads = new Thread[configuration.threadCount()];

	for (int i = 0; i < configuration.threadCount() && i < configuration.varTo(); i++) {
	    (threads[i] = new SequenceSumThread(configuration.varFrom() + i * configuration.varStep(),
		    configuration.varTo(), configuration.threadCount() * configuration.varStep(), (result) -> {
			postResult(result);
		    }, sequence.getMember())).start();
	}

	try {
	    for (Thread thread : threads) {
		thread.join();
	    }
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}

	return sum;
    }

    private synchronized void postResult(Apfloat calculatedValue) {
	sum = sum.add(calculatedValue);
    }

}
