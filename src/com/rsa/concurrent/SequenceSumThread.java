package com.rsa.concurrent;

import org.apfloat.Apfloat;

import com.rsa.math.seq.SequenceMember;

public class SequenceSumThread extends Thread {

    @FunctionalInterface
    public interface ResultCallback {
	void onResult(Apfloat result);
    }

    private long currentVar;
    private long varTo;
    private long varStep;
    private Apfloat sum;
    private ResultCallback callback;
    private SequenceMember member;

    public SequenceSumThread(long varFrom, long varTo, long varStep, ResultCallback callback, SequenceMember member) {
	currentVar = varFrom;
	this.varTo = varTo;
	this.varStep = varStep;
	this.callback = callback;
	this.member = member;
    }

    @Override
    public void run() {
	if (member == null) {
	    return;
	}

	sum = Apfloat.ZERO;
	while (currentVar <= varTo) {
	    sum = sum.add(member.apply(currentVar));
	    currentVar += varStep;
	}
	if (callback != null) {
	    callback.onResult(sum);

	}
    }

}
